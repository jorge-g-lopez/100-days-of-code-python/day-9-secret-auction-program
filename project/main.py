"""Secret Auction Program"""

from art import LOGO

print(LOGO)

print("Welcome to the secret auction program")

bidders = {}
more_bidders = "yes"
highest_name = ""
highest_bid = 0

while more_bidders == "yes":
    name = input("What is your name?: ")
    bid = int(input("What's your bid?: $"))

    bidders[name] = bid
    more_bidders = input("Are there any other biders? Type 'yes' or 'no'. ")
    print("")

for name in bidders:
    if bidders[name] > highest_bid:
        highest_name = name
        highest_bid = bidders[name]

print(f"The winner is {highest_name} with a bid of ${highest_bid}.")
